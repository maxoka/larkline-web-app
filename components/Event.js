import Link from 'next/link'

const Event = ({ event }) => {
    let image = `/images/${event.type}.jpg`;
    return (
        <Link href="event/[id]" as={`/event/${event.id}`}>
        <div className="max-w-xs rounded shadow-lg overflow-hidden my-4 mr-6">
            <img className="h-48 w-full" src={image} alt="basketball court" />
            <div className="bg-yellow-500 h-14 pt-2 align-text-bottom">
                <h2 className="uppercase text-white text-center text-3xl font-bold place-self-center">{event.type}</h2>
            </div>
            <div className="bg-white pb-5">
                <div className="flex items-center mx-4 py-2 border-b-2 border-black">
                    <div className="flex-1"><p className="uppercase text-md text-center text-black font-semibold">{event.location}</p></div>
                    <div className="w-8 h-8 ml-auto flex-none items-center justify-center">
                        <img src="/images/location-pin.png" alt="location pin" />
                    </div>
                </div>
                <div className="flex flex-wrap justify-center align-center py-2">
                    <div className="uppercase text-center text-md font-semibold px-2 border-r-2 border-yellow-500">Host: {event.host}</div>
                    <div className="uppercase text-center text-md font-semibold px-2 border-r-2 border-yellow-500">Time: {event.time}</div>
                    <div className="uppercase text-center text-md font-semibold px-2 border-r-2 border-yellow-500">Players: {event.currPlayers}/{event.maxPlayers}</div>
                    <div className="uppercase text-center text-md font-semibold px-2">AGRP: {event.agrp}</div>
                </div>
                <div className="flex justify-center">
                    <button className="bg-green-400 py-1 px-4 align-text-middle">
                        <p className="text-center text-white font-semibold">I'M IN!</p>
                    </button>
                </div>
                
            </div>
        </div>
        </Link>
    )
}

export default Event
