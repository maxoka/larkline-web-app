import eventStyles from '../styles/Event.module.css'
import Event from './Event'

const EventList = ({ events }) => {
    return (
        <div className="flex flex-row flex-wrap items-center">
            {events.map(event => 
                <Event key={event.id} event={event}/>
            )}
        </div>
    )
}

export default EventList
