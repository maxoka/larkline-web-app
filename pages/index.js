import Head from 'next/head'
import Header from '../components/Header'

export default function Home() {
  return (
    <div className="px-8 mt-10">
      <Head>
        <title>Larkline</title>
        <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
      </Head>   
      <Header />
    </div>
  )
}

