import Head from 'next/head'

const team = () => {
    return (
        <div>
            <Head>
                <title>Larkline - Team</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head> 
            <h1>Team</h1>
        </div>
    )
}

export default team