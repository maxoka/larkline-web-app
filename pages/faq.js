import Head from 'next/head'

const faq = () => {
    return (
        <div>
            <Head>
                <title>Larkline - FAQ</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head>
            <h1>FAQ</h1>
        </div>
    )
}

export default faq