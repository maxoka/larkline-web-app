import Head from 'next/head'

const app = () => {
    return (
        <div>
            <Head>
                <title>Larkline App</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head>
            <h1>App page</h1>
        </div>
    )
}

export default app