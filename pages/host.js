import {server} from '../config'
import Head from 'next/head'
import EventList from '../components/EventList'

const host = ({ events }) => {
    return (
        <div>
            <Head>
                <title>Larkline - Host</title>
                <meta name='keywords' content='event planning, sports, intramurals, recreation, recreation' />
            </Head> 

            <EventList events={events} />

        </div>
    )
}

export const getStaticProps = async () => {
    const res = await fetch(`${server}/api/events`)
    const events = await res.json()
  
    return {
      props: {
        events
      }
    }
  }
  

export default host