export const events = [
    {
        id: "1",
        type: "BASKETBALL",
        location: "HUMBOLDT PARK",
        host: "RILEY",
        time: "2PM",
        currPlayers: "7",
        maxPlayers: "10",
        agrp: "ALL"
    },
    {
        id: "2",
        type: "SKATEBOARD",
        location: "VALLEY PARK",
        host: "KYLE",
        time: "5PM",
        currPlayers: "4",
        maxPlayers: "5",
        agrp: "ALL"
    },
    {
        id: "3",
        type: "FOOTBALL",
        location: "GREEN PARK",
        host: "JAKE",
        time: "1PM",
        currPlayers: "3",
        maxPlayers: "14",
        agrp: "17-22"
    }
]